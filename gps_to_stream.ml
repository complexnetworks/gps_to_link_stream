(* ocamlopt -o gps_to_stream str.cmxa unix.cmxa gps_to_stream.ml *)


(* *** inputs definition *** *)

let in_data = Sys.argv.(1);;
let in_range = Sys.argv.(2);;
let in_dlim = Sys.argv.(3);;


(* *** general toolbox *** *)

let ios x = int_of_string x;;
let fos x = float_of_string x;;
let iof x = int_of_float x;;
let foi x = float_of_int x;;


(* *** building x,y table from GPS data *** *)

(* check that the content of a data line is not empty *)

let checker s = (s <> "" || s = "");;

(* make a list of strings from a data line *)

let reader s =
  let r = Str.regexp "[ \t]+" in
  let l = Str.split r s in
  l;;

(* count the number of lines of the data file *)

let max_line name =
  let data = open_in name in
  let maximum = ref 0 in
  begin
    try
      while (checker (input_line data) = true) do
	maximum := !maximum +1;
      done;
    with | End_of_file -> close_in data;
  end;
  !maximum;;

(* build the table from the data *)

let build_table name nb_lines =
  let loc_table = Array.make_matrix nb_lines 4 (-1.) in
  let data = open_in name in
  let line_nb = ref 0 in
  let ref_lat = ref 0. in
  let ref_lng = ref 0. in
  begin
    try
      while true do
	begin
	  let line = ref (reader (input_line data)) in
	  loc_table.(!line_nb).(0) <- fos (List.hd !line);
	  line := List.tl !line;
	  loc_table.(!line_nb).(1) <- fos (List.hd !line);
	  line := List.tl !line;
	  
	  (* define a reference for latitude  on the first data line *)
	  if (!line_nb == 0) then ref_lat := fos (List.hd !line);
	  (* convert latitude *)
	  let lat = (fos (List.hd !line)) -. !ref_lat in
	  loc_table.(!line_nb).(2) <- lat; 
	  line := List.tl !line;
	  
	  (* define a reference for longitude  on the first data line *)
	  if (!line_nb == 0) then ref_lng := fos (List.hd !line);
	  (* convert longitude *)
	  let lng = ((fos (List.hd !line)) -. !ref_lng) *. (cos (!ref_lat *. 3.14159 /. 180.)) in
	  loc_table.(!line_nb).(3) <- lng; 
	  line_nb := !line_nb +1;
	end
      done;
    with | End_of_file -> close_in data;
  end;
  loc_table;;


(* *** build stream from x,y table *** *)

(* count the maximum id and maximum time in the dataset *)

let max_id_time loc_table =
  let length = Array.length loc_table in
  let id_max = ref 0 in
  let time_max = ref 0 in
  for i=0 to length-1
  do
    if (!id_max < (iof loc_table.(i).(0)))
    then id_max := (iof loc_table.(i).(0));
    if (!time_max < (iof loc_table.(i).(1)))
    then time_max := (iof loc_table.(i).(1));
  done;
  (!id_max , !time_max);;

(* build stream *)

let print_tab arr =
  Array.iter (fun x -> print_float x; print_char '\t') arr;;

let print_tabtab arr =
  Array.iter (fun x -> print_tab x; print_char '\n') arr;;

let build_stream loc_table range dlim id_max time_max =
  let last_active_time_pos = Array.make_matrix (id_max+1) 3 (-1000.) in
  let current_time = ref 0 in  
  let loc_table_cursor = ref 0 in
  let length_deg_lng0 = 111250. in (* length in meters of a longitude degree at the equator *) 
  while (!current_time < time_max)
  do
    begin
      while ((iof loc_table.(!loc_table_cursor).(1)) == !current_time)
      do
	begin
	  let node = (iof loc_table.(!loc_table_cursor).(0)) in
	  let y = loc_table.(!loc_table_cursor).(2) in
	  let x = loc_table.(!loc_table_cursor).(3) in
	  last_active_time_pos.(node).(0) <- (foi !current_time);
	  last_active_time_pos.(node).(1) <- y;
	  last_active_time_pos.(node).(2) <- x;
	  loc_table_cursor := !loc_table_cursor +1;
	end
      done;

      for u=0 to (id_max -1)
      do
	if ((iof last_active_time_pos.(u).(0)) >= (!current_time - dlim))
	then
	  for v=(u+1) to (id_max)
	  do
	    if ((iof last_active_time_pos.(v).(0)) >= (!current_time - dlim))
	    then
	      begin
      		let delta_y = last_active_time_pos.(u).(1) -. last_active_time_pos.(v).(1) in
		let delta_x = last_active_time_pos.(u).(2) -. last_active_time_pos.(v).(2) in
		let distance = length_deg_lng0 *. sqrt (delta_x *. delta_x +. delta_y *. delta_y) in
		if (distance <= (foi range))
		then
		  begin
		    print_int u;
		    print_string "\t";
		    print_int v;
		    print_string "\t";
		    print_int !current_time;
		    print_endline "";
		  end
	      end
	  done;
      done;
  
      current_time := !current_time +1;
    end
  done;;


(* *** implement on inputs *** *)

let in_nb_lines = max_line in_data in
let in_loc_table = build_table in_data in_nb_lines in
let in_max_id_time = max_id_time in_loc_table in
build_stream in_loc_table (ios in_range) (ios in_dlim) (fst in_max_id_time) (snd in_max_id_time);; 

