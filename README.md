gps_to_stream.ml
================
Generate a link stream from GPS data, using the following assumptions:

* there is a contact between two nodes (individuals, vehicles, animals, etc) when the distance between them is < range (integer parameter)
* the location of a node is supposed to be the one at the last time when it has been measured, only if this timestamp occured less than dlim time units before (integer parameter)
* the curvature of the Earth can be neglected in the distance computations (the code uses an approximate haversine formula, which assumes this hypothesis)

These assumptions are not always realistic, here are a few cases where they are not:

* the directions should be taken into account, for example in the case of a wifi packets exchange network, where the waves cannot go through buildings
* the duration between two measurements is long, for example in a vehicular network, the second assumption would be incorrect if the positions are given every minute 

Additional remarks:

* the code is not optimal, especially as it does a comprehensive search on all pairs of active nodes at each timestep, while we could compute the distance only between "close enough" nodes
* the unit of time should be chosen adequately, as we compute distances at each time unit, it implies that if it is chosen too small, there are useless computations
* repeated links will occur a lot if the location of a node is not updated at each time step


# Compilation

To compile, you need an Ocaml compiler ocamlc or ocamlopt, see
http://ocaml.org/

To generate the executable file gps_to_stream from the source file gps_to_stream.ml , type:
```
ocamlopt -o gps_to_stream str.cmxa unix.cmxa gps_to_stream.ml
```
or
```
ocamlc -o gps_to_stream str.cma unix.cma gps_to_stream.ml
```

# Usage, inputs, output
```
./gps_to_stream  [gps_file_name] [range] [dlim]
```

* gps_file_name is a string, the input file itself should have the following format:
```
[id] [time] [lat] [lng]
```
__Be careful that the file is ordered by time and that time starts at time 0.__

* range is an integer (unit : meters)
* dlim is an integer (unit : the time unit of the input file)

* the output file has the following format:
```
[id1] [id2] [time]
```

# Example  
```
./gps_to_stream taxi_Roma.dat 30 10
```